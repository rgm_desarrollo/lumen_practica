<?php

use App\Http\Controllers\AutoresController;



/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () {
    return 'Hello World';
});
 

$router->group(['prefix' => 'api'], function () use ($router) {
$router->get('/autores', 'AutoresController@index');
$router->get('/autores/{id}', 'AutoresController@autorid');
$router->post('/autores', 'AutoresController@create');
$router->put('/autores/{id}', 'AutoresController@update');
$router->delete('/autores/{id}', 'AutoresController@delete');
});


//forma de llamar a la api desde posman  http://localhost:8082/api/autores