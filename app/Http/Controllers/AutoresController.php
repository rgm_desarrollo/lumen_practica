<?php

namespace App\Http\Controllers;

use App\Models\Autores;
use Illuminate\Http\Request;

class AutoresController extends Controller
{

   
    public function index()
    {
        return response()->json(Autores::all());
    }

    public function autorid($id)
    {
        return response()->json(Autores::find($id));
    }

    public function create(Request $request)
    {
        $autores = Autores::create($request->all());
        return response()->json($autores, 201);
    }

    public function update($id, Request $request)
    {
        $autor = Autores::findOrFail($id);
        $autor->update($request->all());
        return response()->json($autor, 200);
    }

    public function delete($id)
    {
        Autores::findorFail($id)->delete();
        return response('Autor elminado correctamente', 200);
    }
}
